const joi = require('joi');

const UsuarioValidacion = joi.object({
    EmailUsuario: joi.string()
        .email({
            minDomainSegments: 2,
            tlds: { allow: ['com','net','co'] },
        }),
    Nombre: joi
        .string()
        .min(3)
        .max(30)
        .required(),
    Contraseña: joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    Repetir_Contraseña: joi.ref('Contraseña'),
    Direccion: joi
        .string()
        .min(3)
        .max(30)
        .required(),
    esAdmin: joi.boolean(),
});

module.exports = UsuarioValidacion;