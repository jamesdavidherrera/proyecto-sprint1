const joi = require('joi');

const Datosvalidos = joi.object({
    Nombre: joi
        .string()
        .min(3)
        .max(30)
        .required(),
    Precio: joi
        .number()
        .required(),
    Disponible: joi
        .boolean()
        .required(),
});

module.exports = Datosvalidos;