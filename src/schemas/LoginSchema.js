const joi = require('joi');

const UsuarioLogin = joi.object({
    EmailUsuario: joi.string()
        .email({
            minDomainSegments: 2,
            tlds: { allow: ['com','net','co'] },
        }),
    Contraseña: joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

});

module.exports = UsuarioLogin;