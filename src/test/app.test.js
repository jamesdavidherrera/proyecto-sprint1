const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../index");
const Usuario = require("../models/users.model");

chai.should();
chai.use(chaiHttp);


describe("Registro usuarios", ()=>{
describe("POST/usuarios/crear", ()=>{
    it("Forma exitosa debe devolver un 200 en status", (done)=>{
        const UsuarioT = {
            EmailUsuario:"test@test.com",
            Nombre:"test",
            Contraseña:"test",
            Direccion: "test-sur"
        }
        chai.request(app)
            .post("/usuarios/crear")
            .send(UsuarioT)
            .end((err, response)=>{
                response.should.have.status(200);
                done();
            });
    });
    it("Forma erronea por el nombre debe devolver un 400 en status", (done)=>{
        const UsuarioT = {
            EmailUsuario:"test@test.com",
            Nombre:"t",
            Contraseña:"test",
            Direccion: "test-sur"
        }
        chai.request(app)
            .post("/usuarios/crear")
            .send(UsuarioT)
            .end((err, response)=>{
                response.should.have.status(400);
                done();
            });
    });
    it("Forma erronea por el final del correo debe devolver un 400 en status", (done)=>{
        const UsuarioT = {
            EmailUsuario:"test@test.llll",
            Nombre:"test",
            Contraseña:"test",
            Direccion: "test-sur"
        }
        chai.request(app)
            .post("/usuarios/crear")
            .send(UsuarioT)
            .end((err, response)=>{
                response.should.have.status(400);
                done();
            });
    });
    it("Forma erronea por la contraseña debe devolver un 400 en status", (done)=>{
        const UsuarioT = {
            EmailUsuario:"test@test.llll",
            Nombre:"test",
            Contraseña:"test$$$",
            Direccion: "test-sur"
        }
        chai.request(app)
            .post("/usuarios/crear")
            .send(UsuarioT)
            .end((err, response)=>{
                response.should.have.status(400);
                done();
            });
    });
    after(async()=>{
        await Usuario.deleteOne({EmailUsuario:"test@test.com"});

    });
});
});

describe("Iniciar Sesion usuarios", ()=>{
    describe("POST/usuarios/iniciarSesion", ()=>{
        it("Forma exitosa debe devolver un 200 en status", (done)=>{
            const UsuarioT = {
                EmailUsuario:"james@gmail.com",
                Contraseña:"123456789",
            }
            chai.request(app)
                .post("/usuarios/iniciarSesion")
                .send(UsuarioT)
                .end((err, response)=>{
                    response.should.have.status(200);
                    done();
                });
        });
        it("Forma erronea por Email debe devolver un 400 en status", (done)=>{
            const UsuarioT = {
                EmailUsuario:"jamess@gmail",
                Contraseña:"123456789",
            }
            chai.request(app)
                .post("/usuarios/iniciarSesion")
                .send(UsuarioT)
                .end((err, response)=>{
                    response.should.have.status(400);
                    done();
                });
        });
        it("Forma erronea por contraseña debe devolver un 400 en status", (done)=>{
            const UsuarioT = {
                EmailUsuario:"james@gmail.com",
                Contraseña:"1000621219",
            }
            chai.request(app)
                .post("/usuarios/iniciarSesion")
                .send(UsuarioT)
                .end((err, response)=>{
                    response.should.have.status(400);
                    done();
                });
        });
    });
    });