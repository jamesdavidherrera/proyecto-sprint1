const express = require("express");
const app = express();
const Helmet = require("helmet")
const expressJwt = require("express-jwt");
const Swaggerjsdocs = require("swagger-jsdoc");
const SwaggerUI = require("swagger-ui-express");
const SwaggerOptions = require("./utils/SwaggerOptions");
require("dotenv").config();
require("./db");
require("./Creacion");
const Contraseña = 'gQi5h5dFU45c1Vdx2dvq'

const useusersroute = require("./routes/user.route");
const useproductsroute = require("./routes/product.route");
const useordersroute = require("./routes/order.route");
const usepaymentsroute = require("./routes/payment.route");

const SwaggerSpects = Swaggerjsdocs(SwaggerOptions);
app.use("/api-docts", SwaggerUI.serve, SwaggerUI.setup(SwaggerSpects));

app.use(Helmet());
app.use(expressJwt({
    secret: Contraseña,
    algorithms: ['HS256'],}).unless({path: ['/usuarios/iniciarSesion', '/usuarios/crear']}),
);
app.use(express.json());

app.use("/usuarios", useusersroute);
app.use("/productos",useproductsroute);
app.use("/pagos",usepaymentsroute);
app.use("/ordenes",useordersroute);

app.listen(3014,()=>{ console.log("Escuchando en el puerto 3014")});

module.exports = app;