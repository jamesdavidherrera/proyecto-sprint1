const express = require("express");
const routerorder = express.Router();
const Orden = require("../models/orders.model");
const Producto = require("../models/products.model");
const {esAdmin} = require("../middlewares/Usuarios.middleware");
const {PedidoP} = require("../middlewares/Ordenes.middleware");
const {ExisteProducto} = require("../middlewares/Productos.middleware");
const {ExistePago} = require("../middlewares/Pagos.middleware");

/**
 * @swagger
 * /ordenes:
 *  get:
 *      summary: Obtener todos los pedidos
 *      tags: [Pedidos]
 *      responses:
 *                200:
 *                    description: Lista de pedidos
 *                401: 
 *                    description: debe iniciar sesion como administrador
 */
routerorder.get("/",esAdmin,async(req,res)=>{
    const Ordenes = await Orden.find();
    res.json(Ordenes);
});

/**
 * @swagger
 * /ordenes/mispedidos:
 *  get:
 *      summary: Obtener mis pedidos 
 *      tags: [Pedidos]
 *      responses:
 *          '200':
 *              description: pedidos del usuario
 *          '401':
 *              description: debe iniciar sesion
 */

routerorder.get("/mispedidos",async(req,res)=>{
    const {EmailUsuario} = req.user;
    const HistorialMisPedidos = await Orden.find({EmailUsuario});
    if(HistorialMisPedidos.length===0){
        res.json("No tiene pedidos, Perfecto puede crear uno, Vamos animate!!!")
    }else res.json(HistorialMisPedidos);
});

/**
 * @swagger
 * /ordenes/crear:
 *  post:
 *      summary: Crear un pedido
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          pago:
 *                              type: string
 *                      example:
 *                          pago: Efectivo
 *      tags: [Pedidos]
 *      responses:
 *           200:
 *              description: Pedido creado exitosamente
 *           400:
 *              description: Este usuario ya tiene un pedido abierto, confirmelo para poder crear uno nuevo
 *           411:
 *              description: Debes completar los campos solicitados o tal vez no has iniciado sesion 
 */

routerorder.post("/crear",ExistePago, async(req,res)=>{
    const {EmailUsuario,Direccion} = req.user;
    const {pago} = req.body;
    const Productos = [];
    const Total = 1;
    let aux = true;

   const PedidoAbierto = await Orden.findOne({EmailUsuario, Estado: 'ABIERTO'});
   const PedidoPendiente = await Orden.findOne({EmailUsuario, Estado: 'PENDIENTE'});
   if(PedidoAbierto) {
       res.status(400).json("Este usuario ya tiene un pedido abierto, confirmelo para poder crear uno nuevo");
       aux = false;
    } else aux = true;
   if(PedidoPendiente) {
       res.status(400).json("Este usuario ya tiene un pedido pendiente, confirmelo para poder crear uno nuevo");
       aux = false;
   } else aux = true;
   if(aux){
       if(Direccion, pago, EmailUsuario, Total){
           const NuevoPedido = new Orden({EmailUsuario,Direccion,pago,Productos,Total});
           NuevoPedido.save(); 
           res.json([{Msj:"Pedido Creado Añade productos"},NuevoPedido]);
        }else res.status(411).json("Debes completar los campos solicitados, o tal vez no has iniciado sesion")
    }
});

/**
 * @swagger
 * /ordenes/agregar/producto:
 *  put:
 *      summary: Agregar producto
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          Productoo:
 *                              type: string
 *                      example:
 *                          Productoo: PIZZA 
 *      tags: [Pedidos]
 *      responses:
 *          '200':
 *              description: Producto añadido
 *          '302':
 *              description: Cantidad del producto alterada
 *          '404':
 *              description: No tienes pedidos abiertos
 */

routerorder.put("/agregar/producto", ExisteProducto, async(req,res)=>{
    const {EmailUsuario} = req.user;
    const {Productoo} = req.body; 
    let aux = 0;
    const PedidoA = await Orden.findOne({EmailUsuario, Estado:"ABIERTO"});
    const ProductoNuevo = await Producto.findOne({Nombre: Productoo});
    try {
    const PedidoP = await Orden.findOne({EmailUsuario, Estado:"PENDIENTE"});
    if(PedidoA){
        PedidoA.Productos.push(ProductoNuevo);
        Total = ProductoNuevo.Precio + PedidoA.Total - 1;
        PedidoA.Total = Total;
        PedidoA.Estado = "PENDIENTE"; 
        PedidoA.save();
        res.json([{msg:"Producto añadido"},PedidoA]);
    }
    if(PedidoP){
        const Productos = PedidoP.Productos;
        for(i=0; i<Productos.length; i++){
            const ProductoA = Productos[i];
            if(ProductoA.Nombre == ProductoNuevo.Nombre){
                ProductoA.Cantidad = ProductoA.Cantidad + 1;
                Total = ProductoA.Precio + (PedidoP.Total);
                PedidoP.Total=Total;
                aux = aux + 1;
                PedidoP.save();
                res.status(302).json([{msg:"Cantidad del producto alterada"},PedidoP]);                
            }
        }
        if(aux === 0){
            PedidoP.Productos.push(ProductoNuevo);
            Total = ProductoNuevo.Precio + PedidoP.Total;
            PedidoP.Total = Total;
            PedidoP.save();
            res.json([{msg:"Producto añadido"},PedidoP]);  
        }; 
    }else res.status(404).json("No tienes pedidos abiertos");
    }catch(error){res.status(404).json("Producto no encontrado")};
});

/**
 * @swagger
 * /ordenes/eliminar/producto:
 *  delete:
 *      summary: Eliminar producto
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          Productoo:
 *                              type: string
 *                      example:
 *                          Productoo: PIZZA 
 *      tags: [Pedidos]
 *      responses:
 *          '200':
 *              description: Producto eliminado
 *          '404':
 *              description: Este producto no esta en tu pedido
 */

routerorder.delete("/eliminar/producto", ExisteProducto,PedidoP, async(req,res)=>{
    const {EmailUsuario} = req.user;
    const {Productoo} = req.body; 
    const ProductoE = await Producto.findOne({Nombre: Productoo});
    const PedidoP = await Orden.findOne({EmailUsuario, Estado:"PENDIENTE"});
    const Productos = PedidoP.Productos;
    let aux = 0;
    for(i=0; i<Productos.length; i++){
        const ProductoA = Productos[i];
        if(ProductoA.Nombre === ProductoE.Nombre){
            ProductoA.Cantidad = ProductoA.Cantidad - 1;
            if(ProductoA.Cantidad===0){
                const index = Productos.findIndex(A => A === ProductoA);
                Productos.splice(index,1);
                Total =  PedidoP.Total - ProductoA.Precio;
                PedidoP.Total=Total;
                PedidoP.save();
                res.json([{msg:"Producto eliminado, ya no esta en tu pedido"},PedidoP]);
            }else
            Total =  PedidoP.Total - ProductoA.Precio;
            PedidoP.Total=Total;
            await PedidoP.save();
            res.json([{msg:"Producto eliminado"},PedidoP]);
        }else aux = aux + 1;
    };
    if(aux===Productos.length){res.status(404).json("Este producto no esta en tu pedido")};
});

/**
 * @swagger
 * /ordenes/confirmar:
 *  put:
 *      summary: Confirmar pedido
 *      tags: [Pedidos]
 *      responses:
 *           200:
 *              description: Pedido confirmado
 *           400: 
 *              description: Tu pedido no se puede confirmar ya que aun no esta PENDIENTE, para lograr esto agrega Productos
 *           404:
 *              description: No tienes pedidos abiertos
 */

routerorder.put("/confirmar",async(req,res)=>{
    const {EmailUsuario} = req.user;
    const Pedidos = await Orden.find({EmailUsuario});
    const Pedido = await Orden.findOne({EmailUsuario, Estado:"PENDIENTE"});
    if(Pedidos.length>0){
        if(Pedido){
            Pedido.Estado = "CONFIRMADO";
            Pedido.save();
            res.json(Pedido);
        }else  res.status(400).json("Tu pedido no se puede confirmar ya que aun no esta PENDIENTE, para lograr esto agrega Productos");
    }else res.status(404).json("No tienes pedidos abiertos");
});

/**
 * @swagger
 * /ordenes/actualizar/estado/{_id}:
 *  put:
 *      summary: Actualizar estado de un pedido por su id
 *      parameters:
 *         - name: _id
 *           in: path
 *           required: true
 *           description: Id del pedido
 *           schema:
 *                type: string
 *      requestBody:
 *            required: true
 *            content:
 *                application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              NuevoEstado:
 *                                   type: string
 *                          example:
 *                              NuevoEstado: PENDIENTE
 *      tags: [Pedidos]
 *      responses:
 *             200:
 *                description: Estado del pedido actualizado
 *             400:
 *                description: El estado de la orden solo puede ser PENDIENTE,CONFIRMADO,EN PREPARACION,ENVIADO,ENTREGADO
 *             401:
 *                description: Debe iniciar sesion como administrador
 *             404:
 *                description: pedido no encontrado
 */

routerorder.put("/actualizar/estado/:_id",esAdmin,async(req,res)=>{
    const {_id} = req.params;
    try{
    const {NuevoEstado} = req.body;
    const Pedido = await Orden.findById(_id);
    if(Pedido){
        if(NuevoEstado == "PENDIENTE" || NuevoEstado =="CONFIRMADO" || NuevoEstado =="EN PREPARACION" || NuevoEstado =="ENVIADO" || NuevoEstado =="ENTREGADO"){
        Pedido.Estado = NuevoEstado;
        Pedido.save();
        res.json(Pedido);
        }else res.status(400).json("El estado de la orden solo puede ser PENDIENTE,CONFIRMADO,EN PREPARACION,ENVIADO,ENTREGADO")
    }else res.status(404).json("Pedido no encontrado");
    }catch(error){res.status(404).json("Pedido no encontrado")};
});
    
/**
 * @swagger
 * tags:
 *   name: Pedidos
 *   descriptions: Seccion de pedidos
 * 
 * components:
 *   schemas:
 *        pedidos:
 *            type: object
 *            required:
 *                -Id
 *                -Productos
 *                -Estado
 *                -EmailUsuario
 *                -Direccion
 *                -Pago
 *            properties:
 *                Id:
 *                   type: integer
 *                   description: id del pedido
 *                productos:
 *                   type: array
 *                   description: array de los productos
 *                Estado:
 *                   type: string
 *                   description: Estado del producto
 *                Emailusuario:
 *                   type: string
 *                   description: Email del cliente
 *                Direccion:
 *                   type: string
 *                   description: Direccion del cliente
 *                Pago:
 *                   type: string
 *                   description: Metodo de pago
 *                Total:
 *                   type: number
 *                   description: Total a pagar 
 *            example:
 *                Id: 1
 *                Products: [{Id: 1, name: "Arepa", price: 2000, Disponible: true, Cantidad: 2}]
 *                Estado: PENDIENTE
 *                EmailUsuario: andres@gmail.com
 *                Direccion: Cr 11A Sur #20 - 04
 *                Pago: efectivo
 *                Total: 4000                
 */

module.exports = routerorder;