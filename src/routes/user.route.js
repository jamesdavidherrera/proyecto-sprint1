const express = require("express");
const routeruser = express.Router();
const bcrypt = require('bcrypt');
const Usuario = require("../models/users.model");
const jsonwebtoken = require('jsonwebtoken');
const {esAdmin, InicioSesion, AutenticacionJWT, UsuariosDuplicados} =require("../middlewares/Usuarios.middleware");

/**
 * @swagger
 * /usuarios:
 *  get:
 *      summary: Obtener todos los usuarios
 *      tags: [Usuarios]
 *      responses:
 *                200:
 *                    description: Lista de usuarios del sistema
 *                401: 
 *                    descrption: Debe iniciar sesion como administrador
 */
routeruser.get("/",esAdmin, async(req,res)=>{
 const Usuarios = await Usuario.find();
 res.json(Usuarios);
});


/**
 * @swagger
 * /usuarios/crear:
 *  post:
 *      summary: Crear un usuario nuevo
 *      security: []
 *      requestBody:
 *            required: true
 *            content:
 *                application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                             EmailUsuario:
 *                                 type: string
 *                             Nombre:
 *                                 type: string
 *                             Contraseña: 
 *                                 type: string
 *                             Direccion: 
 *                                 type: string
 *                          example:
 *                             EmailUsuario: pepito@gmail.com
 *                             Nombre: Pepito Perez
 *                             Contraseña: pepepe159
 *                             Repetir_Contraseña: pepepe159
 *                             Direccion: 436 E St NW
 *      tags: [Usuarios]
 *      responses:
 *               200:
 *                   description: Usuario creado  
 *               400: 
 *                   description: Todos los campos son necesarios
 *               403:
 *                   description: El usuario ya existe, no es posible crearlo
 * 
 */

routeruser.post("/crear",AutenticacionJWT,UsuariosDuplicados,(req,res)=>{
const {EmailUsuario, Nombre, Contraseña, Direccion, esAdmin} = req.body;
if(esAdmin){
    if (EmailUsuario, Nombre, Contraseña, Direccion, esAdmin){
        const NuevoUsuario = new Usuario({EmailUsuario, Nombre, Contraseña: bcrypt.hashSync(Contraseña, 10), Direccion, esAdmin});
        NuevoUsuario.save();
        res.json(NuevoUsuario);
    }else res.status(400).json("Todos los campos son necesarios");
}else if (EmailUsuario, Nombre, Contraseña, Direccion){
          const NuevoUsuario = new Usuario({EmailUsuario, Nombre, Contraseña: bcrypt.hashSync(Contraseña, 10), Direccion})
          NuevoUsuario.save();
          res.json(NuevoUsuario);
          }else res.status(400).json("Todos los campos son necesarios");
});


/**
 * @swagger
 * /usuarios/iniciarSesion:
 *  post:
 *      summary: Iniciar Sesion en la pagina 
 *      security: []
 *      requestBody:
 *            required: true
 *            content:
 *                application/json:
 *                      schema:
 *                           type: object
 *                           properties:
 *                                EmailUsuario:
 *                                    type: string
 *                                Contraseña: 
 *                                    type: string
 *                      example:
 *                          EmailUsuario: pepito@gmail.com
 *                          Contraseña: "pepepe159"
 *      tags: [Usuarios]
 *      responses:
 *               200:
 *                  description: Inicio de sesion exitoso  
 *               400: 
 *                  description: Usuario o contraseña incorrectos 
 */
routeruser.post("/iniciarSesion",InicioSesion, async(req,res)=>{
    const {EmailUsuario} = req.body;
    const {Direccion, esAdmin} = await Usuario.findOne({EmailUsuario});
    const Token = jsonwebtoken.sign({EmailUsuario, Direccion, esAdmin},"gQi5h5dFU45c1Vdx2dvq");
    res.json({Token});
});
/**
 * @swagger
 * tags:
 *   name: Usuarios
 *   descriptions: Seccion de usuarios
 * 
 * components:
 *   schemas:
 *        usuario:
 *            type: object
 *            properties:
 *                 EmailUsuario:
 *                      type: string
 *                      description: Email del usuario
 *                 Nombre:
 *                      type: string
 *                      description: Nombre del usuario
 *                 Contraseña: 
 *                      type: string
 *                      description: Contraseña del usuario
 *                 Direccion: 
 *                      type: string
 *                      description: Direccion del usuario
 *            example:
 *                 EmailUsuario: pepito@gmail.com
 *                 Nombre: Pepito Perez
 *                 Contraseña: pepepe159
 *                 Direccion: 436 E St NW     
 */

module.exports = routeruser;