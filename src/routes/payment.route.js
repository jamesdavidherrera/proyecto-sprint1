const express = require("express");
const routerpayment = express();
const Pago = require("../models/payments.model");
const {esAdmin} = require("../middlewares/Usuarios.middleware");
const {ExistePago, PagosDuplicados} = require("../middlewares/Pagos.middleware");


/**
 * @swagger
 * /pagos:
 *  get:
 *      summary: Obtener todos los medios de pago
 *      tags: [Pagos]
 *      responses:
 *                200:
 *                    description: Lista de formas de pago del sistema
 *                401: 
 *                    description: debe iniciar sesion  
 */
routerpayment.get("/",async (req,res)=>{
    const Pagos = await Pago.find();
    res.json(Pagos);
});

/**
 * @swagger
 * /pagos/crear:
 *  post:
 *      summary: Crear una forma de pago nueva
 *      requestBody:
 *            required: true
 *            content:
 *                application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                             Nombre:
 *                                 type: string
 *                          example:
 *                               Nombre: Botones
 *      tags: [Pagos]
 *      responses:
 *               200:
 *                   description: Forma de pago creada  
 *               401: 
 *                   descrption: Debe iniciar sesion como administrador 
 */
routerpayment.post("/crear",esAdmin,PagosDuplicados, async(req,res)=>{
const Nombre = req.body.Nombre;
const NuevoPago = new Pago({Nombre});
NuevoPago.save();
res.json(NuevoPago);
});

/**
 * @swagger
 * /pagos/actualizar/{_id}:
 *  put:
 *      summary: Actualizar una forma de pago por su id
 *      parameters:
 *         - name: _id
 *           in: path
 *           required: true
 *           description: id del medio de pago
 *           schema:
 *                type: string
 *      requestBody:
 *            required: true
 *            content:
 *                application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              Nombre:
 *                                  type: string
 *                          example:
 *                              Nombre: Efectivo COP
 *      tags: [Pagos]
 *      responses:
 *             200:
 *                  description: Forma de pago Actualizada 
 *             401: 
 *                  description: Debe iniciar sesion como administrador
 *             404:  
 *                  description: Medio de pago no encontrado
 */
routerpayment.put("/actualizar/:_id",esAdmin,ExistePago,async(req,res)=>{
    const { _id } = req.params;
    const { Nombre } = req.body; 
    const ActualizarMetodoPago = await Pago.findById(_id);
    ActualizarMetodoPago.Nombre = Nombre;
    ActualizarMetodoPago.save();
    res.json(ActualizarMetodoPago);
});

/**
 * @swagger
 * /pagos/eliminar/{_id}:
 *  delete:
 *      summary: eliminar una forma de pago
 *      parameters:
 *          - name: _id
 *            in: path
 *            required: true
 *            description: id de la forma de pago
 *            schema:
 *                 type: string
 *      tags: [Pagos]
 *      responses:
 *            200:
 *                description: Forma de pago eliminada 
 *            401: 
 *                description: Debe iniciar sesion como administrador
 *            404:  
 *                description: Medio de pago no encontrado
 */
routerpayment.delete("/eliminar/:_id",esAdmin,ExistePago, async(req,res)=>{
    const { _id } = req.params;
    const EliminarPago = await Pago.findByIdAndDelete(_id);
    res.json([{msj:"Se elimino el sigiente medio de pago"}, EliminarPago]);
});

/**
 * @swagger
 * tags:
 *   name: Pagos
 *   descriptions: Seccion de Pagos
 * 
 * components:
 *   schemas:
 *        pagos:
 *            type: object
 *            properties:
 *                Id:
 *                   type: integer
 *                   description: id del pedido
 *                Nombre:
 *                   type: string
 *                   description: Nombre del medio de pago
 *            example:
 *                   id: 124581425
 *                   Nombre: Efectivo
 */

module.exports=routerpayment;