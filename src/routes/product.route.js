const express = require("express");
const routerproduct = express.Router();
const redis = require("redis");
/** const clienteRedis = redis.createClient(6379);*/
const {ProductosDuplicados, Cache, ExisteProducto, Datosverificados} = require("../middlewares/Productos.middleware");
const Producto = require("../models/products.model");
const { esAdmin } = require("../middlewares/Usuarios.middleware");



/**
 * @swagger
 * /productos:
 *  get:
 *      summary: Obtener todos los productos
 *      tags: [Productos]
 *      responses:
 *                200:
 *                    description: Lista de productos del sistema
 *                401: 
 *                    description: debe iniciar sesion  
 */
routerproduct.get("/",/**Cache,*/async(req,res)=>{
    const Productos = await Producto.find();
    /** clienteRedis.setex("http://localhost:3014/productos",230, JSON.stringify(Productos));*/
    res.json(Productos);
});

/**
 * @swagger
 * /productos/crear:
 *  post:
 *      summary: Crear un producto nuevo
 *      requestBody:
 *            required: true
 *            content:
 *                application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                             Nombre:
 *                                 type: string
 *                             Precio:
 *                                 type: number
 *                             Disponible:
 *                                 type: Boolean
 *                          example:
 *                               Nombre: Combo con papas
 *                               Precio: 6000
 *                               Disponible: true
 *      tags: [Productos]
 *      responses:
 *               200:
 *                   description: Producto creado  
 *               400: 
 *                   description: Todos los campos son necesarios
 *               401: 
 *                   description: Debe iniciar sesion como administrador
 *               403: 
 *                   description: El Producto ya existe, no es posible crearlo
 */
routerproduct.post("/crear",esAdmin,ProductosDuplicados,Datosverificados,(req,res)=>{
    const {Nombre, Precio, Disponible} = req.body;
    if(Nombre, Precio, Disponible){
       const NuevoProducto = new Producto({Nombre, Precio, Disponible});
       NuevoProducto.save();
       /**clienteRedis.del("http://localhost:3014/productos");*/
       res.json(NuevoProducto);
    }else res.status(400).json({err: "todos los datos son necesarios"});
});

/**
 * @swagger
 * /productos/actualizar/{_id}:
 *  put:
 *      summary: Actualizar un producto por su id
 *      parameters:
 *         - name: _id
 *           in: path
 *           required: true
 *           description: Id del producto
 *           schema:
 *                type: string
 *      requestBody:
 *            required: true
 *            content:
 *                application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              Nombre:
 *                                  type: string
 *                              Precio:
 *                                  type: number
 *                              Disponible:
 *                                 type: Boolean
 *                          example:
 *                              Nombre: Cocacola flexi
 *                              Precio: 2500
 *                              Disponible: true        
 *      tags: [Productos]
 *      responses:
 *             200:
 *                 description: Producto Actualizado
 *             400: 
 *                 description: Producto no encontrado  
 *             401: 
 *                 description: Debe iniciar sesion como administrador
 *             404: 
 *                 description: Actualizaciones no encontradas
 */
routerproduct.put("/actualizar/:_id",esAdmin, ExisteProducto, async(req,res)=>{
    const {_id} = req.params;
    const { Nombre, Precio, Disponible } = req.body;
    const ActualizarProducto = await Producto.findById(_id);
    let aux = 0;
    if(Nombre){
    ActualizarProducto.Nombre = Nombre
    aux += 1;
    };
    if(Precio){
    ActualizarProducto.Precio = Precio;
    aux += 1;
    };
    if(Disponible){
    ActualizarProducto.Disponible = Disponible; 
    aux += 1;   
    };
    if(aux>0){
    ActualizarProducto.save();
   /** clienteRedis.del("http://localhost:3014/productos");*/
    res.json(ActualizarProducto);
    }else res.status(404).json("Actualizaciones no encontradas");
});

/**
 * @swagger
 * /productos/eliminar/{_id}:
 *  delete:
 *      summary: Eliminar un producto
 *      parameters:
 *          - name: _id
 *            in: path
 *            required: true
 *            description: Id del producto
 *            schema:
 *                 type: string
 *      tags: [Productos]
 *      responses:
 *            200:
 *                description: Producto Eliminado
 *            400: 
 *                description: Producto no encontrado
 *            401: 
 *                description: Debe iniciar sesion como administrador  
 */
routerproduct.delete("/eliminar/:_id",esAdmin, ExisteProducto, async(req,res)=>{
    const {_id} = req.params;
    const EliminarProducto = await Producto.findByIdAndDelete(_id);
    /**clienteRedis.del("http://localhost:3014/productos");*/
    res.json([{Msj:"Se elimino el sigiente producto"}, EliminarProducto]);
});
/**
 * @swagger
 * tags:
 *   name: Productos
 *   descriptions: Seccion de productos
 * 
 * components:
 *   schemas:
 *        productos:
 *            type: object
 *            properties:
 *               Nombre:
 *                   type: string
 *                   description: Nombre del producto
 *               Precio:
 *                    type: integre
 *                    description: Precio del producto
 *               Disponible:
 *                    type: boolean
 *                    description: Disponibilidad del producto 
 *            example:
 *                 Nombre: HamburguesaSuper
 *                 Precio: 12000
 *                 Disponible: true   
 */

module.exports=routerproduct;
