const SwaggerOptions = {
    definition:{
        openapi: "3.0.1",
        info:{
            title: "Proyecto Sprint 3 James Herrera",
            version: "4.1",
            description: "Proyecto para acamica"
        },
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: "http",
                    scheme: "bearer"
                }
            }
        },
        security: [
            {
                bearerAuth: []
            }
        ]
    },
    apis:["./src/routes/*.js"]
}

module.exports = SwaggerOptions;
