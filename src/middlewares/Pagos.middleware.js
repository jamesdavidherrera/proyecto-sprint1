const mongoose = require("mongoose");
const Pago = require("../models/payments.model");

const ExistePago = async(req,res,next)=>{
    try{
    const { _id } = req.params;
    const { pago } = req.body;
    if(_id){
        const Pagoo = await Pago.findById(_id);
        Pagoo ? next() : res.status(404).json("Medio de pago no encontrado");
    };
    if(pago){
        const Pagoo = await Pago.findOne({Nombre:pago});
        Pagoo ? next() : res.status(404).json("Medio de pago no encontrado");
    };
    }
    catch(error){res.status(400).json("Medio de pago no encontrado")};
};

const PagosDuplicados = async(req, res, next) =>{
    const {Nombre} = req.body;
    const Existe = await Pago.findOne({Nombre});
    Existe ? res.status(403).json("El medio de pago ya existe, no es posible crearlo") : next();
};

module.exports = {ExistePago, PagosDuplicados};