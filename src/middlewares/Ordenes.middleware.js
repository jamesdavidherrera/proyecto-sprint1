const mongoose = require("mongoose");
const Orden = require("../models/orders.model");

const PedidoP = async(req,res,next)=>{
    const {EmailUsuario} = req.user;
    const PedidoP = await Orden.findOne({EmailUsuario, Estado:"PENDIENTE"});
    PedidoP ? next() : res.status(400).json("No puedes eliminar productos de un pedido confirmado O tal vez no tienes pedidos abiertos");
};

module.exports = {PedidoP}; 