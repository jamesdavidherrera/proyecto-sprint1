const mongoose = require("mongoose");
const Producto = require("../models/products.model");
const redis = require("redis");
const Datosvalidos = require("../schemas/Product.Schema");
/**const clienteRedis = redis.createClient(6379);*/

const Datosverificados = async(req, res, next) =>{
    try{
        const {Nombre, Precio, Disponible} = 
        await Datosvalidos.validateAsync(req.body);
        next();
    }catch(error){res.status(400).json(error)};
};

/**const Cache = (req,res,next) =>{
    clienteRedis.get("http://ec2-35-153-71-240.compute-1.amazonaws.com/productos",(err,Productos)=>{
        if(err) throw err;
        if(Productos){
            res.json(JSON.parse(Productos));
        }else next();
    });
};
*/
const ExisteProducto = async(req, res, next) =>{
    try {
    const {Productoo} = req.body;
    const {_id} = req.params;
    if(Productoo){
        const ProductoNuevo = await Producto.findOne({Nombre: Productoo});
        if(ProductoNuevo){
            ProductoNuevo.Disponible ? next() : res.status(403).json("El producto que intenta agregar no esta disponible en el momento intentelo mas tarde o comuniquese con el administrador");
        }else res.status(400).json("El Producto insertado no existe");
    };
    if(_id){
        const ProductoNuevo = await Producto.findById(_id);
        ProductoNuevo ? next() : res.status(400).json("Producto no encontrado"); 
    }; 
    }
    catch(error){res.status(404).json("Producto no encontrado")};
};

const ProductosDuplicados = async(req, res, next) =>{
    const {Nombre} = req.body;
    const Existe = await Producto.findOne({Nombre});
    Existe ? res.status(403).json("El Producto ya existe, no es posible crearlo") : next();
};


module.exports = {ExisteProducto, ProductosDuplicados,/**Cache,*/Datosverificados};
