const bcrypt = require('bcrypt');
const mongoose = require("mongoose");
const Usuario = require("../models/users.model");
const UsuarioValidacion = require("../schemas/RegistroSchema");
const UsuarioLogin = require("../schemas/LoginSchema");

const UsuariosDuplicados = async(req, res, next) =>{
    const {EmailUsuario} = req.body;
    const Existe = await Usuario.find({EmailUsuario});
    if(Existe.length >= 1) 
    {res.status(403).json("El usuario ya existe, no es posible crearlo")}
    else next();
};

const AutenticacionJWT = async(req, res, next) =>{
    try{
        const {EmailUsuario, Nombre, Contraseña, Direccion, esAdmin} = 
        await UsuarioValidacion.validateAsync(req.body);
        next();
    }
    catch (error) {res.status(400).json(error.details)};
};

const InicioSesion = async(req, res, next) =>{
    try{
        const {EmailUsuario, Contraseña} = 
        await UsuarioLogin.validateAsync(req.body);
        const Perfil = await Usuario.findOne({EmailUsuario});
        const resultado = bcrypt.compareSync(Contraseña, Perfil.Contraseña); 
        if(resultado){
            next();
        }else{
            res.status(400).json("Usuario o Contraseña erroneos");
        }
    }
    catch(error){res.status(400).json(error)};
}

const esAdmin = async(req, res, next)=>{
    try{
    const {esAdmin} = req.user;
    esAdmin ? next() : res.status(401).json("No es administrador");
    }
    catch(error){res.status(400).json(error)};
};

module.exports = {UsuariosDuplicados, esAdmin, AutenticacionJWT, InicioSesion};