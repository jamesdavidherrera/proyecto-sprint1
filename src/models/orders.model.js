const mongoose = require("mongoose");

const ordenSchema = new mongoose.Schema({
EmailUsuario: {
type: String,
required: true
},
Direccion:{
type: String,
require: true
},
pago:{
type: String,
required: true
},
Estado:{
type: String,
default: 'ABIERTO'
},
Productos:[
{
  Nombre:{
    type: String,
    required: true
  },
  Precio:{
    type: Number,
    required: true
  },
  Cantidad:{
    type: Number,
    default: 1
  }
}
],
Total:{
type: Number,
required: true
}
});

module.exports = mongoose.model("Orden",ordenSchema);