const mongoose = require("mongoose");

const ProductosSchema = new mongoose.Schema({
    Nombre:{
        type: String,
        required: true
    },
    Precio:{
        type: Number,
        required: true
    },
    Disponible:{
        type: Boolean,
        default: true
    },
    Cantidad:{
        type: Number,
        default: 1
    },
}); 

module.exports = mongoose.model("Producto", ProductosSchema)