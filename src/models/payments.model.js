const mongoose = require("mongoose");

const PagosSchema = new mongoose.Schema({
    Nombre:{
        type: String,
        required: true
    }
});

module.exports = mongoose.model("Pago",PagosSchema);