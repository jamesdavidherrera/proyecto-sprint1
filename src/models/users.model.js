const mongoose = require("mongoose");

const UsuarioSchema = new mongoose.Schema({
    EmailUsuario: {
        type: String,
        required: true
    },
    Nombre:{
        type: String,
        require: true
    },
    Contraseña:{
        type: String,
        required: true
    },
    Direccion:{
        type: String,
        require: true
    },
    esAdmin:{
        type: Boolean,
        default: false
    }
});

module.exports= mongoose.model("Usuario",UsuarioSchema);
