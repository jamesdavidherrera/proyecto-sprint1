const mongoose = require("mongoose");
const bcrypt = require('bcrypt');
const Usuario = require("./models/users.model");
const Producto = require("./models/products.model");
const Pago = require("./models/payments.model");

(async()=>{
    const Usuarios = await Usuario.find();
    if(Usuarios.length===0){
        const EmailUsuario = "james@gmail.com";
        const Nombre = "james";
        const Contraseña = "123456789";
        const Direccion = "Tv14 D Este #58 Sur-04";
        const esAdmin = true;
        const NuevoUsuario = new Usuario({EmailUsuario, Nombre, Contraseña: bcrypt.hashSync(Contraseña, 10), Direccion, esAdmin});
        NuevoUsuario.save();
    };
    const Productos = await Producto.find();
    if(Productos.length===0){
        const Nombre = "PIZZA";
        const Precio = "3200";
        const Disponible = true;
        const NuevoProducto = new Producto({Nombre, Precio, Disponible});
        NuevoProducto.save();
    };
    const Pagos = await Pago.find();
    if(Pagos.length===0){
        const Nombre = "Efectivo";
        const NuevoPago = new Pago({Nombre});
        NuevoPago.save();
    };
})();